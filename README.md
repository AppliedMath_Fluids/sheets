# HTML renders of workbooks.


This git repository is the home of the files served from
https://wiki.math.uwaterloo.ca/sheets

Please understand these worksheets can not be ensured to be AODA complient because:
 - data is submited from users not employees
 - the HTML render engins in matlab, jupyter etc.. are missing flexibility

If you find any issues not related to the content of the worksheets feel free to submit an issue report to:
https://git.uwaterloo.ca/math/sheets/issues

------------------------

# Adding new worksheets


If you wish to add or change worksheets please submit a pull request to:
https://git.uwaterloo.ca/math/sheets

------------------------

# Hosting files from FluidsWiki

This repository can be used to host files on the Fluids Wiki (wiki.math.uwaterloo.ca/fluidswiki).
This can be particularly useful for sharing larger code files.

To host files on the fluids wiki, first add them to this repository.
When doing so, please try to keep the directory structure clean: group similarly themed 
files together in sub-directories, consider adding README files in subdirectories
to outline contents.

Anything to be hosted on the FluidsWiki should be kept under the FluidsWiki directory.

## Hosting file online

Afterwards, submit a merge request to the main repository (https://git.uwaterloo.ca/math/sheets, 
or follow the fork link in the project description).

## Linking from the repo

Once the file is available on wiki.math.uwaterloo.ca/sheets (may need to contact Steve Weber), you can link to it from the Fluids wiki.

For example, to link to the main README file, you would use the following: `[https://wiki.math.uwaterloo.ca/sheets/README.md Link text]`