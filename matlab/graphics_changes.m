%% Matlab Graphics Changes in 2014b onwards
% In 2014b MathWorks made major changes to the way that graphics works in
% MATLAB. In general, these changes can be summarized as graphic objects
% working like other types of objects. For example, a "graphics object" (a
% figure) will now correspond to the object handle rather than to a double.
%%
% 
% Eg. fig1 = contour(peaks, 100)
%   Now returns an object handle 'fig1' which can be used along with "dot
%   notation" to access properties (eg. fig1.Color='red';). Before this
%   same command would return a double, thus any code which took advantage
%   of this and performed arithmetic operations on the handles will not
%   work in the new system.
%%
% Previously to change graphics properties you would have to use the "get"
% and "set" functions (set(fig1,'Color','red')), this method will still
% work.
%%
% By default MATLAB will use OpenGL as the graphics engine. This can still
% be set manually to be different.
%%
% For more specifics about the changes that were made, please visit:
% http://www.mathworks.com/help/matlab/graphics-changes-in-r2014b.html 

